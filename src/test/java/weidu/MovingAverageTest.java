package weidu;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.*;

public class  MovingAverageTest{
    
    public Double[] array = {11.0, 10.0, 11.0, 10.0, 11.0, 10.0, 9.0, 10.0, 11.0, 13.0, 14.0, 15.0, 10.0, 8.0, 6.0, 5.0};
    
    public List<Double> input = new ArrayList<Double>(Arrays.asList(array));
    
    public static final double DELTA = 1e-2;
    @Test
    public void twoDayShort() 
        throws Exception
    {    
        MovingAverage val = new MovingAverage();
        
        Double[] twoDayArr = {-1.0, 10.5, 10.5, 10.5, 10.5, 10.5, 9.5, 9.5, 10.5, 12.0, 13.5, 14.5, 12.5, 9.0, 7.0, 5.5};
        
        List<Double> twoDayOutput = new ArrayList<Double>(Arrays.asList(twoDayArr));
        
        assertEquals(twoDayOutput, val.cal(input, 2));
    }
    
    @Test
    public void threeDayShort() 
        throws Exception
    {    
        MovingAverage val = new MovingAverage();
        
        Double[] threeDayArr = {-1.0, -1.0, 10.66, 10.33, 10.66, 10.33, 10.0, 9.6, 10.0, 11.3, 12.6, 14.0, 13.0, 11.0, 8.0, 6.3};
        
        List<Double> threeDayOutput = new ArrayList<Double>(Arrays.asList (threeDayArr));
        
        assertEquals(threeDayOutput, val.cal(input, 3));
    }
    
    @Test
    public void fourDayShort() 
        throws Exception
    {    
        MovingAverage val = new MovingAverage();
        
        Double[] fourDayArr = {-1.0, -1.0, -1.0, 10.5, 10.5, 10.5, 10.0, 10.0, 10.0, 10.75, 12.0, 13.25, 13.0, 11.75, 9.75, 7.25};
        
        List<Double> fourDayOutput = new ArrayList<Double>(Arrays.asList (fourDayArr));
        
        assertEquals(fourDayOutput, val.cal(input, 4));
    }
    
    @Test
    public void fiveDayShort() 
        throws Exception
    {    
        MovingAverage val = new MovingAverage();
        
        Double[] fiveDayArr = {-1.0, -1.0, -1.0, -1.0, 10.6, 10.4, 10.2, 10.0, 10.2, 10.6, 11.4, 12.6, 12.6, 12.0, 10.6, 8.8};
        
        List<Double> fiveDayOutput = new ArrayList<Double>(Arrays.asList (fiveDayArr));
        
        assertEquals(fiveDayOutput, val.cal(input, 5));
    }
    
    @Test
    public void sixDayShort() 
        throws Exception
    {    
        MovingAverage val = new MovingAverage();
        
        Double[] sixDayArr = {-1.0, -1.0, -1.0, -1.0, -1.0, 10.5, 10.16, 10.16, 10.16, 10.16, 11.16, 12.0, 12.16, 11.83, 11.0, 9.66};
        
        List<Double> sixDayOutput = new ArrayList<Double>(Arrays.asList (sixDayArr));
        
        assertEquals(sixDayOutput, val.cal(input, 6));
    }
    
    @Test
    public void sevenDayShort() 
        throws Exception
    {    
        MovingAverage val = new MovingAverage();
        
        Double[] sevenDayArr = {-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, 10.28, 10.14, 10.28, 10.57, 11.14, 11.71, 11.71, 11.57, 11.0, 10.14};
        
        List<Double> sevenDayOutput = new ArrayList<Double>(Arrays.asList (sevenDayArr));
        
        assertEquals(sevenDayOutput, val.cal(input, 7));
    }
    
    @Test
    public void eightDayShort() 
        throws Exception
    {    
        MovingAverage val = new MovingAverage();
        
        Double[] eightDayArr = {-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, 10.25, 10.25, 10.625, 11.0, 11.625, 11.5, 11.25, 10.875, 10.25};
        
        List<Double> eightDayOutput = new ArrayList<Double>(Arrays.asList (eightDayArr));
        
        assertEquals(eightDayOutput, val.cal(input,8));
    }
    
    @Test
    public void nineDayShort() 
        throws Exception
    {    
        MovingAverage val = new MovingAverage();
        
        double[] nineDayArr = {-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, 10.33, 10.55, 11.0, 11.44, 11.44, 11.11, 10.66, 10.22};
        
        List<Double> result = val.cal(input,9);
        double[] target = new double[result.size()];
        for (int i = 0; i < target.length; i++) {
           target[i] = result.get(i);                // java 1.5+ style (outboxing)
        }
      
        assertArrayEquals(nineDayArr, target,DELTA);
    }
    
    @Test
    public void tenDayShort() 
        throws Exception
    {    
        MovingAverage val = new MovingAverage();
        
        Double[] tenDayArr = {-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, 10.6, 10.9, 11.4, 11.3, 11.1, 10.6, 10.};
        
        List<Double> tenDayOutput = new ArrayList<Double>(Arrays.asList (tenDayArr));
        
        assertEquals(tenDayOutput, val.cal(input,10));
    }
}