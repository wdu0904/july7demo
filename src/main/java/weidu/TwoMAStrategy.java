package weidu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TwoMAStrategy {
	
	public static String twoMAS(List<Double> list, int s, int l){
		List<Double> shorts = MovingAverage.cal(list, s);
		List<Double> longs = MovingAverage.cal(list, l);
		int i = 0;
		
		while (i < shorts.size() && (shorts.get(i)< 0 || longs.get(i)<0)){
			i++;
		}
		if (i>= shorts.size()){
			return "No cross";
		}
		
		String action = " BUY";
		
		if (shorts.get(i) < longs.get(i)){
			while (i < shorts.size() && shorts.get(i) <= longs.get(i)){
				i++;
			}
		} else {
			action = " SELL";
			while (i < shorts.size() && shorts.get(i) >= longs.get(i)){
				i++;
			}
		}
		
		if (i>= shorts.size()){
			return "No cross";
		}
		return action + " @ " + (i+1);
	}
	
}


