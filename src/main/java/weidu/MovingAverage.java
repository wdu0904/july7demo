package weidu;

import java.util.ArrayList;
import java.util.List;

public class MovingAverage {
    public static List<Double> cal(List<Double> stream, int days){
        int i = 0;
        double buffer = 0;
        double avg = 0;
        List<Double> result = new ArrayList<Double>();
        while(i < stream.size()){
            double number = stream.get (i);
            if(i+1 < days){
            buffer = buffer + number;
            result.add ((double) -1);
            }
            else if(i+1 == days){
                buffer = buffer + number;
                avg = buffer/days;
                result.add (avg);
            }
            else{
                buffer = buffer + number - stream.get(i-days);
                avg = buffer/days;
                result.add (avg);
            }
            i++;
        }
        return result;
    }
}
